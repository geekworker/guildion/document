import DataStoreImpl from './datastore_impl';
const singleton = Symbol();
const singletonEnforcer = Symbol();

export default class DocumentDataStore extends DataStoreImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DocumentDataStore(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async sync({ document }) {
        const data = await super.apiCall('/api/v1/document/sync', {
            document,
        });
        return data.document;
    }

    async syncSection({ documentSection }) {
        const data = await super.apiCall('/api/v1/document/section/sync', {
            documentSection,
        });
        return data.documentSection;
    }

    async syncGroup({ documentGroup }) {
        const data = await super.apiCall('/api/v1/document/group/sync', {
            documentGroup,
        });
        return data.documentGroup;
    }

    async hides({ document_uids }) {
        const data = await super.apiCall('/api/v1/documents/hide', {
            document_uids,
        });
        return true;
    }

    async hideSections({ documentSection_uids }) {
        const data = await super.apiCall('/api/v1/document/sections/hide', {
            documentSection_uids,
        });
        return true;
    }

    async hideGroups({ documentGroup_uids }) {
        const data = await super.apiCall('/api/v1/document/groups/hide', {
            documentGroup_uids,
        });
        return true;
    }
}
