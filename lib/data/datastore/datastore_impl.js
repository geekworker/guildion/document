import fetch from 'isomorphic-fetch';
import config from '../../application/constants/config';
import path from 'path';

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'all';

export default class DataStoreImpl {
    constructor() {}

    async apiCall(pathname, payload, reqType = 'POST') {
        const _path = config.CURRENT_APP_URL + pathname;
        payload.api_key = process.env.API_KEY;
        payload.api_password = process.env.API_PASSWORD;
        const reqObjs = {
            POST: {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(payload),
            },
            PUT: {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
            GET: {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(payload),
            },
        };
        logger.debug(`API SESSION --> ${reqType} ${_path} `);
        const response = await fetch(_path, reqObjs[reqType]).catch(e => {
            throw e;
        });
        const contentType = response.headers.get('content-type');
        if (!contentType || contentType.indexOf('application/json') === -1) {
            throw new Error('Invalid response from server');
        }
        const responseData = await response.json();
        logger.debug(`API SESSION <-- ${reqType} ${_path} `);
        return responseData;
    }
}
