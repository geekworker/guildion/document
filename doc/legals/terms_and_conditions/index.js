const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../../..', 'lib', 'models', ...rest);
}

const models = require(resolveModelsPath());

module.exports = models.DocumentSection.build({
    ja_title: '利用規約',
    en_title: 'Terms and Conditions',
    index: 0,
    template: null,
    permission: true,
});
