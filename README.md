# Guildion Document Controller

##### Community building service for movie watchers ( https://guildion.us )

This project for easy to make guildion document pages by one command

## Useage

#### Clone the repository and make a tmp folder

````
git clone https://gitlab.com/Zakimiii/guildion-document
cd guildion-document
````

### Install dependencies on Javascript

````
nvm install v8.7
npm install -g yarn
yarn global add babel-cli
yarn install --frozen-lockfile
yarn run build
````

### To sync documents with localhost Guildion in development mode

```
sudo yarn run dev
```

### To sync documents with Guildion in production mode

```
sudo yarn run prod
```

