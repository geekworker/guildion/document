const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../../../..', 'lib', 'models', ...rest);
}

function resolvePath(...rest) {
    return path.join(__dirname, ...rest);
}

const readFile = (filename) => {
    return fs.readFileSync(resolvePath(filename), 'utf8');
}

const models = require(resolveModelsPath());

const buildDocument = () => {
    const ja_html = readFile(`_ja.html`);
    const en_html = readFile(`_en.html`);
    return models.Document.build({
        ja_title: "第二条 収集する個人情報",
        en_title: "2. Personal Data to be collected",
        ja_html,
        en_html,
        index: 2,
        template: null,
        permission: true,
    })
}

module.exports = buildDocument()
