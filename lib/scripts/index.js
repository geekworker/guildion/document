import log4js from 'log4js';
import path from 'path';
import DocumentRepository from '../data/repository/document_repository';

const logger = log4js.getLogger();
logger.level = 'all';

Object.assign(process.env, {
    NODE_PATH: path.resolve(__dirname, '../..'),
    ...require('dotenv').config({
        path: `.env.${process.env.NODE_ENV}`,
    }).parsed,
});

const run = async () => {
    await DocumentRepository.instance.sync_all_documents()
}

run();
