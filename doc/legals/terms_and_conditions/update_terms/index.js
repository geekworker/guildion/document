const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../../../..', 'lib', 'models', ...rest);
}

function resolvePath(...rest) {
    return path.join(__dirname, ...rest);
}

const readFile = (filename) => {
    return fs.readFileSync(resolvePath(filename), 'utf8');
}

const models = require(resolveModelsPath());

const buildDocument = () => {
    const ja_html = readFile(`_ja.html`);
    const en_html = readFile(`_en.html`);
    return models.Document.build({
        ja_title: "第三条 規約の更新・変更",
        en_title: "3. Modification to these Terms and Conditions",
        ja_html,
        en_html,
        index: 3,
        template: null,
        permission: true,
    })
}

module.exports = buildDocument()
