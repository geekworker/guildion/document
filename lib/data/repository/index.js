import RepositoryImpl from './repository_impl';
import DocumentRepository from './document_repository';

module.exports = {
    RepositoryImpl,
    DocumentRepository,
};
