const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../..', 'lib', 'models', ...rest);
}

const models = require(resolveModelsPath());

module.exports = models.DocumentGroup.build({
    ja_title: 'サポートセンター',
    en_title: 'Support Center',
    template: null,
    permission: true,
});
