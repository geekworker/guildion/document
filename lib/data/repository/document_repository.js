import RepositoryImpl from './repository_impl';
import DocumentDataStore from '../datastore/document_datastore';

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'all';

const singleton = Symbol();
const singletonEnforcer = Symbol();

const documentDataStore = DocumentDataStore.instance;

export default class DocumentRepository extends RepositoryImpl {
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new DocumentRepository(singletonEnforcer);
        }
        return this[singleton];
    }

    constructor(enforcer) {
        super();
        if (enforcer != singletonEnforcer) throw 'Cannot construct singleton';
    }

    async sync(document) {
        logger.debug(`🌈 DOCUMENT SYNC START: [${document.group_uid}/${document.section_uid}/${document.uid}]`);
        await documentDataStore.sync({ document });
        logger.debug(`✅ DOCUMENT SYNC SUCCESS: [${document.group_uid}/${document.section_uid}/${document.uid}]`);
    }

    async sync_section(section, documents) {
        logger.debug(`🌕 SECTION SYNC START: [${section.group_uid}/${section.uid}]`);
        await documentDataStore.syncSection({ documentSection: section });
        for (const [index, document] of documents.entries()) {
            await this.sync(document);
        }
        logger.debug(`✅ SECTION SYNC SUCCESS: [${section.group_uid}/${section.uid}]`);
    }

    async sync_group(group, sections, d_documents) {
        logger.debug(`🌏 GROUP SYNC START: [${group.uid}]`);
        await documentDataStore.syncGroup({ documentGroup: group });
        for (const [index, section] of sections.entries()) {
            await this.sync_section(section, d_documents[index]);
        }
        logger.debug(`💯 GROUP SYNC SUCCESS: [${group.uid}]`);
    }

    async sync_all_documents() {
        const {
            groups,
            d_sections,
            dd_documents,
        } = await require('../../builder')();

        for (const [index, group] of groups.entries()) {
            await this.sync_group(group, d_sections[index], dd_documents[index]);
        }
        const d_documents = Array.prototype.concat.apply([], dd_documents);
        const documents = Array.prototype.concat.apply([], d_documents);
        const sections = Array.prototype.concat.apply([], d_sections);
        await documentDataStore.hides({ document_uids: documents.map(val => val.uid) });
        await documentDataStore.hideSections({ documentSection_uids: sections.map(val => val.uid) });
        await documentDataStore.hideGroups({ documentGroup_uids: groups.map(val => val.uid) });
        logger.debug(`🎊 ALL SYNC SUCCESS`);
    }
}
