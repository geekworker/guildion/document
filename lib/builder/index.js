import fs from 'fs';
import path from 'path';
import models from '../models';

function resolveDocPath(...rest) {
    return path.join(__dirname, '../..', 'doc', ...rest);
}

const readDocList = () => {
    return fs.readdirSync(resolveDocPath(), { withFileTypes: true })
            .filter(dirent => dirent.isDirectory())
            .map(dirent => dirent.name)
}

const readDirList = (foldername) => {
    return fs.readdirSync(resolveDocPath(foldername), { withFileTypes: true })
            .filter(dirent => dirent.isDirectory())
            .map(dir => dir.name)
}

const readFile = (filename) => {
    return new Promise((resolve, reject) => {
        fs.readFile(resolveDocPath(filename), 'utf8', (err, words) => {
            if (err) {
                reject(err);
            } else {
                resolve(words);
            }
        });
    });
}

const buildDocument = async (foldername, section, group) => {
    const document = require(resolveDocPath(`${group.uid}/${section.uid}/${foldername}`));
    document.group_uid = group.uid
    document.section_uid = section.uid
    document.uid = foldername
    return document
}

const buildDocuments = async (section, group) => {
    const documentLists = readDirList(`${group.uid}/${section.uid}`);
    const documents = await Promise.all(
        documentLists.map(name => buildDocument(name, section, group))
    );
    return documents
}

const buildSection = async (foldername, group) => {
    const section = require(resolveDocPath(`${group.uid}/${foldername}`));
    section.group_uid = group.uid;
    section.uid = foldername;
    return section
}

const buildSections = async (group) => {
    const groupLists = readDirList(group.uid);
    const sections = await Promise.all(
        groupLists.map(name => buildSection(name, group))
    );
    return sections
}

const buildGroup = async (foldername) => {
    const group = require(resolveDocPath(foldername));
    group.uid = foldername;
    group.groupname = foldername;
    return group
}

const build = async () => {
    const docList = readDocList();
    const groups = await Promise.all(
        docList.map(name => buildGroup(name))
    );
    const d_sections = await Promise.all(
        groups.map(group => buildSections(group))
    );
    const dd_documents = await Promise.all(
        groups.map(async(group, i) => {
            return await Promise.all(
                d_sections[i].map(section => buildDocuments(section, group))
            );
        })
    );

    return {
        groups,
        d_sections,
        dd_documents,
    }
}

module.exports = build;
