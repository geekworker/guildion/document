const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../../../..', 'lib', 'models', ...rest);
}

function resolvePath(...rest) {
    return path.join(__dirname, ...rest);
}

const readFile = (filename) => {
    return fs.readFileSync(resolvePath(filename), 'utf8');
}

const models = require(resolveModelsPath());

const buildDocument = () => {
    const ja_html = readFile(`_ja.html`);
    const en_html = readFile(`_en.html`);
    return models.Document.build({
        ja_title: "プライバシーポリシー - 目次 -",
        en_title: "Privacy Policies - Outline -",
        ja_html,
        en_html,
        index: 0,
        template: null,
        permission: true,
    })
}

module.exports = buildDocument()
