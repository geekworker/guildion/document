const available_locales = ['ja', 'en'];

const available_locale_labels = ['日本語', 'English'];

module.exports = {
    available_locales,
    available_locale_labels,
};
