const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../../..', 'lib', 'models', ...rest);
}

const models = require(resolveModelsPath());

module.exports = models.DocumentSection.build({
    ja_title: 'お問い合わせ',
    en_title: 'Contact',
    index: 0,
    template: null,
    permission: true,
});
