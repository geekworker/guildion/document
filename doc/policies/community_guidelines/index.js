const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../../..', 'lib', 'models', ...rest);
}

const models = require(resolveModelsPath());

module.exports = models.DocumentSection.build({
    ja_title: 'コミュニティーガイドライン',
    en_title: 'Community Guideline',
    index: 0,
    template: null,
    permission: true,
});
