const DocumentGroup = {
    build: init => {
        return {
            id: null,
            uid: null,
            groupname: null,
            ja_title: null,
            en_title: null,
            template: null,
            permission: true,
            createdAt: new Date(),
            updatedAt: new Date(),
            ...init,
        };
    },
    is: obj =>
        !!obj &&
        !('SectionId' in obj) &&
        !('GroupId' in obj) &&
        'ja_title' in obj &&
        'en_title' in obj &&
        'groupname' in obj &&
        'template' in obj,
};

module.exports = DocumentGroup;
