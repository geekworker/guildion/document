export const APP_NAME = 'Guildion';
export const APP_NAME_LATIN = 'Guildion';
export const APP_NAME_UPPERCASE = 'GUILDION';
export const APP_ICON = 'Guildion';
export const HOST_NAME = 'Guildion';
export const APP_DOMAIN = 'guildion.us';
export const APP_URL = 'https://guildion.us';
export const LOCAL_APP_URL = 'http://localhost:8080';
export const CURRENT_APP_URL =
    process.env.NODE_ENV == 'production' ? APP_URL : LOCAL_APP_URL;
export const APP_HOST = 'guildion.us';
export const IOS_SCHEME = 'guildion://';

export const IOS_DOWNLOAD_URL =
    'https://apps.apple.com/jp/app/nowful-realtime-community/id1515505548';

module.exports = {
    APP_NAME,
    APP_NAME_LATIN,
    APP_NAME_UPPERCASE,
    APP_ICON,
    APP_URL,
    LOCAL_APP_URL,
    APP_DOMAIN,
    APP_HOST,
    CURRENT_APP_URL,
    IOS_SCHEME,
    IOS_DOWNLOAD_URL,
};
