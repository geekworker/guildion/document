const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../..', 'lib', 'models', ...rest);
}

const models = require(resolveModelsPath());

module.exports = models.DocumentGroup.build({
    ja_title: '規定とポリシー',
    en_title: 'Regulations and Policies',
    template: null,
    permission: true,
});
