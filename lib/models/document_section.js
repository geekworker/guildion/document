const DocumentSection = {
    build: init => {
        return {
            id: null,
            SectionId: null,
            GroupId: null,
            section_uid: null,
            group_uid: null,
            uid: null,
            ja_title: null,
            en_title: null,
            template: null,
            index: null,
            permission: true,
            createdAt: new Date(),
            updatedAt: new Date(),
            ...init,
        };
    },
    is: obj =>
        !!obj &&
        'SectionId' in obj &&
        'GroupId' in obj &&
        'ja_title' in obj &&
        'en_title' in obj &&
        !('ja_html' in obj) &&
        !('en_html' in obj) &&
        !('ja_thumbnail' in obj) &&
        !('en_thumbnail' in obj) &&
        !('version' in obj),
};

module.exports = DocumentSection;
