const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../../..', 'lib', 'models', ...rest);
}

const models = require(resolveModelsPath());

module.exports = models.DocumentSection.build({
    ja_title: '特定商品取引法に基づく表記',
    en_title: 'Act on Specified Commercial Transactions',
    index: 2,
    template: null,
    permission: true,
});
