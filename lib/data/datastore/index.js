import DataStoreImpl from './datastore_impl';
import DocumentDataStore from './document_datastore';

module.exports = {
    DataStoreImpl,
    DocumentDataStore,
};
