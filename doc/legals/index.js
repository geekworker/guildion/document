const fs = require('fs');
const path = require('path');

function resolveModelsPath(...rest) {
    return path.join(__dirname, '../..', 'lib', 'models', ...rest);
}

const models = require(resolveModelsPath());

module.exports = models.DocumentGroup.build({
    ja_title: '法務',
    en_title: 'Regals',
    template: null,
    permission: true,
});
