module.exports = {
    Document: require('./document'),
    DocumentGroup: require('./document_group'),
    DocumentSection: require('./document_section'),
};
